import sys

from confluent_kafka.cimpl import Message, KafkaException

sys.path.append('../../common/src')

from Kafka.KafkaConsumer import KafkaConsumer
from Kafka.KafkaProducer import KafkaProducer

CONSUMER_ID = "Server"
CONSUMER_TOPIC = "ClientToServer"
PRODUCER_TOPIC = "ServerToClient"

class Server:
    def __init__(self):
        self.consumer = KafkaConsumer(CONSUMER_ID)
        self.producer = KafkaProducer()


    def handle_client(self, message: Message):
        print("Received message", message.value().decode(), CONSUMER_TOPIC)
        self.producer.send_message(PRODUCER_TOPIC, message.value().decode()[::-1])
        print("Sent message", PRODUCER_TOPIC)


    def run(self):
        try:
            self.consumer.subscribe([CONSUMER_TOPIC], self.handle_client)
        except KafkaException:
            print("Failed to subscribe to the topic. retrying...")
            self.run()