from actions.Actions import Actions

from actions.message_io_actions import get_message_from_user, print_kafka_message

CONTINUE_MENU = True
STOP_MENU = False
START_FROM = 1
MESSAGE = 0
FUNCTION = 1


class Menu:
    def __init__(self):
        self.actions = Actions(get_message_input_func=get_message_from_user, message_output_func=print_kafka_message)
        self.options = [('Send a new message', self.actions.send_message), ('Exit', self.actions.stop)]

    def __print_menu(self):
        print("MENU:")
        for i in range(1, len(self.options) + START_FROM):
            print(f"{i}. {self.options[i - 1][MESSAGE]}")

    def __execute_option(self, choice):
        self.options[choice - START_FROM][FUNCTION]()

    def __get_option_from_menu(self):
        choice = 0

        while choice < START_FROM or choice > len(self.options):
            try:
                self.__print_menu()
                choice = int(input())
            except ValueError as e:
                print("Invalid input")

        return choice

    def run(self):
        return self.__execute_option(self.__get_option_from_menu())





