import sys
sys.path.append('../../common/src')

from Kafka.KafkaConsumer import KafkaConsumer
from Kafka.KafkaProducer import KafkaProducer

CONSUMER_ID = "PythonClient"
CONSUMER_TOPIC = "ServerToClient"
PRODUCER_TOPIC = "ClientToServer"

class Actions:
    def __init__(self, get_message_input_func, message_output_func):
        self.consumer = KafkaConsumer(CONSUMER_ID)
        self.producer = KafkaProducer()
        self.get_message_input_func = get_message_input_func
        self.get_message_output_func = message_output_func

    def send_message(self):
        try:
            self.producer.send_message(PRODUCER_TOPIC, self.get_message_input_func())
            self.consumer.subscribe([CONSUMER_TOPIC], self.get_message_output_func, consume_once=True)
        except ConnectionError as e:
            print("Failed to communicate with the server due to connection error", e)

    def stop(self):
        self.consumer.unsubscribe()
        exit(0)





