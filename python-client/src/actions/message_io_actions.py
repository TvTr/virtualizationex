from confluent_kafka.cimpl import Message


def get_message_from_user():
    return input("Enter message to send: ")


def print_kafka_message(message: Message):
    message_value = message.value().decode()
    print(message_value)