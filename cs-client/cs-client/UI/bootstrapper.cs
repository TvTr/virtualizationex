using BL.Actions;
using Common.Kafka;
using UI.Actions;
using UI.Actions.Abstract;
using UI.IO;
using UI.IO.Abstract;

namespace UI;

public class Bootstrapper
{
    private const string ClientGroupId = "CSClient";
    private const string ProduceTopic = "ClientToServer";
    private const string ConsumeTopic = "ServerToClient";
    private const string KafkaBroker = "kafka:9092";
    private readonly Menu _menu;
    private readonly MessageSender _messageSender;

    private readonly IReader _reader;
    private readonly IWriter _writer;

    private IDictionary<string, IAsyncAction> _actions;

    public Bootstrapper()
    {
        _reader = new ConsoleReader();
        _writer = new ConsoleWriter();

        var kafkaProducer = new KafkaProducer(KafkaBroker);
        var kafkaConsumer = new KafkaConsumer(ClientGroupId, KafkaBroker);

        _messageSender = new MessageSender(kafkaConsumer, kafkaProducer, ConsumeTopic, ProduceTopic);

        InitializeActions();
        _menu = new Menu(_actions, _reader, _writer);
    }

    public async Task RunAsync()
    {
        await _menu.RunAsync();
    }

    private void InitializeActions()
    {
        _actions = new Dictionary<string, IAsyncAction>
        {
            { "Send Message", new SendMessageAsyncAction(_writer, _reader, _messageSender) },
            { "Exit", new ExitAsyncAction() }
        };
    }
}