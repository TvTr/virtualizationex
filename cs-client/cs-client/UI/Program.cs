﻿namespace UI;

public static class Program
{
    private static void Main(string[] args)
    {
        var bootstrapper = new Bootstrapper();
        Task.WaitAll(bootstrapper.RunAsync());
    }
}