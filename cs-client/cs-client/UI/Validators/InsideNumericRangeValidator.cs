using UI.Validators.Abstract;

namespace UI.Validators;

public class InsideNumericRangeValidator : IValidator
{
    private readonly int _end;
    private readonly int _start;

    public InsideNumericRangeValidator(int start, int end)
    {
        _start = start;
        _end = end;
    }

    public bool Validate(string data)
    {
        try
        {
            var castedData = int.Parse(data);
            return castedData >= _start && castedData <= _end;
        }
        catch (FormatException e)
        {
            return false;
        }
    }
}