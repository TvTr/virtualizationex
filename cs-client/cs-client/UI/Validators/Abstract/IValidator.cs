namespace UI.Validators.Abstract;

public interface IValidator
{
    bool Validate(string data);
}