using UI.Actions.Abstract;
using UI.IO.Abstract;
using UI.Validators;
using UI.Validators.Abstract;

namespace UI;

public class Menu
{
    private const string MenuTitle = "Menu:";
    private const string UserInputMessage = "Enter your choice";
    private const string ExitMessage = "Bye Bye!";

    private readonly IDictionary<string, IAsyncAction> _actions;
    private readonly IReader _reader;
    private readonly IValidator _validator;
    private readonly IWriter _writer;

    public Menu(IDictionary<string, IAsyncAction> actions, IReader reader, IWriter writer)
    {
        _actions = actions;
        _writer = writer;
        _reader = reader;
        _validator = new InsideNumericRangeValidator(1, actions.Count);
    }

    public async Task RunAsync()
    {
        var status = ActionExitStatus.Continue;

        while (status != ActionExitStatus.Abort)
        {
            WriteMenu();
            var userInput = GetUserChoice();
            status = await _actions.ElementAt(userInput - 1).Value.RunAsync();
        }

        _writer.WriteLine(ExitMessage);
    }

    private int GetUserChoice()
    {
        int userInput;

        try
        {
            userInput = int.Parse(_reader.Read(UserInputMessage, _validator));
        }
        catch (FormatException e)
        {
            _writer.WriteLine("Error! Invalid input.");
            userInput = GetUserChoice();
        }

        return userInput;
    }

    private void WriteMenu()
    {
        var index = 1;

        _writer.WriteLine(MenuTitle);
        foreach (var key in _actions.Keys)
        {
            _writer.WriteLine($"{index}.\t{key}");
            index++;
        }
    }
}