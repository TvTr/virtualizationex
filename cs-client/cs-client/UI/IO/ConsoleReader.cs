using UI.IO.Abstract;
using UI.Validators.Abstract;

namespace UI.IO;

public class ConsoleReader : IReader
{
    public string Read()
    {
        return Console.ReadLine() ?? string.Empty;
    }

    public string Read(string message)
    {
        Console.Write($"{message}: ");
        return Read();
    }

    public string Read(string message, IValidator validator)
    {
        var inputValue = Read(message);
        if (!validator.Validate(inputValue)) throw new FormatException("Input value invalid");

        return inputValue;
    }
}