using UI.Validators.Abstract;

namespace UI.IO.Abstract;

public interface IReader
{
    string Read();
    string Read(string message);
    string Read(string message, IValidator validator);
}