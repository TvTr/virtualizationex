using UI.IO.Abstract;

namespace UI.IO;

public class ConsoleWriter : IWriter
{
    public void Write(string message)
    {
        Console.Write(message);
    }

    public void WriteLine(string message)
    {
        Write($"{message}{Environment.NewLine}");
    }
}