namespace UI.Actions.Abstract;

public enum ActionExitStatus
{
    Continue,
    Abort
}