﻿namespace UI.Actions.Abstract;

public interface IAsyncAction
{
    Task<ActionExitStatus> RunAsync();
}