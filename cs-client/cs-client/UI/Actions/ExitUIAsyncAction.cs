﻿using UI.Actions.Abstract;

namespace UI.Actions;

public class ExitAsyncAction : IAsyncAction
{
    public Task<ActionExitStatus> RunAsync()
    {
        return Task.FromResult(ActionExitStatus.Abort);
    }
}