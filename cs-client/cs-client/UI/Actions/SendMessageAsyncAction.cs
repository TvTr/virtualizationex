﻿using BL.Actions;
using UI.Actions.Abstract;
using UI.IO.Abstract;

namespace UI.Actions;

public class SendMessageAsyncAction : IAsyncAction
{
    private readonly string _getMessageTitle;
    private readonly string _messageReceivedTitle;
    private readonly MessageSender _messageSender;
    private readonly IReader _reader;

    private readonly IWriter _writer;

    public SendMessageAsyncAction(
        IWriter writer,
        IReader reader,
        MessageSender messageSender,
        string getMessageTitle = "Enter message to send",
        string messageReceivedTitle = "Received"
    )
    {
        _writer = writer;
        _reader = reader;
        _messageSender = messageSender;
        _getMessageTitle = getMessageTitle;
        _messageReceivedTitle = messageReceivedTitle;
    }

    public async Task<ActionExitStatus> RunAsync()
    {
        try
        {
            var userMessage = _reader.Read(_getMessageTitle);
            var serverMessage = await _messageSender.SendAndReceiveAsync(userMessage);
            _writer.WriteLine($"{_messageReceivedTitle}: {serverMessage}");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
        }

        return ActionExitStatus.Continue;
    }
}