﻿using Common.Kafka;

namespace BL.Actions;

public class MessageSender
{
    private readonly KafkaConsumer _consumer;
    private readonly string _consumeTopic;
    private readonly KafkaProducer _producer;
    private readonly string _produceTopic;

    public MessageSender(KafkaConsumer consumer, KafkaProducer producer, string consumeTopic, string produceTopic)
    {
        _consumer = consumer;
        _producer = producer;
        _produceTopic = produceTopic;
        _consumeTopic = consumeTopic;
    }

    public async Task<string> SendAndReceiveAsync(string message)
    {
        await _producer.SendMessageAsync(_produceTopic, message);
        var result = await _consumer.ReadMessage(_consumeTopic, new CancellationTokenSource());

        return result;
    }
}