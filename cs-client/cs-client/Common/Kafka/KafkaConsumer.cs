using Confluent.Kafka;

namespace Common.Kafka;

public class KafkaConsumer : IDisposable
{
    private readonly IConsumer<Ignore, string> _consumer;

    public KafkaConsumer(string groupId, string bootstrapServers)
    {
        var config = new ConsumerConfig
        {
            BootstrapServers = bootstrapServers,
            GroupId = groupId,
            AutoOffsetReset = AutoOffsetReset.Earliest
        };

        _consumer = new ConsumerBuilder<Ignore, string>(config).Build();
    }

    public void Dispose()
    {
        _consumer.Close();
        _consumer.Dispose();
    }

    public async Task<string> ReadMessage(string topic, CancellationTokenSource cts)
    {
        var result = string.Empty;
        _consumer.Subscribe(topic);

        try
        {
            var consumeTask = Task.Run(() => _consumer.Consume(cts.Token).Message.Value);
            result = await consumeTask;
            _consumer.Unsubscribe();
        }
        catch (OperationCanceledException)
        {
        }

        return result;
    }
}