using Confluent.Kafka;

namespace Common.Kafka;

public class KafkaProducer : IDisposable
{
    private readonly IProducer<Null, string> _producer;

    public KafkaProducer(string bootstrapServers)
    {
        var config = new ProducerConfig
        {
            BootstrapServers = bootstrapServers
        };
        _producer = new ProducerBuilder<Null, string>(config).Build();
    }

    public void Dispose()
    {
        _producer.Dispose();
    }

    public async Task SendMessageAsync(string topic, string data)
    {
        try
        {
            var kafkaMessage = new Message<Null, string> { Value = data };
            await _producer.ProduceAsync(topic, kafkaMessage);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
        }
    }
}