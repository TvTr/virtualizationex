# VirtualizationEx

## Run the code using docker
1. Start the compose using the following command
    ```bash
    docker-compose up -d
    ```
2. It is recommended to wait until the init-kafka container exits as this container will exit only after the Kafka broker is ready to work
3. Now you can start the client:
    - **Python Client**:
       ```bash
        docker exec -it virtualizationex-python-client-1 /bin/bash
        python src/main.py
       ```
   - **C# Client**:
     ```bash
      docker exec -it virtualizationex-cs-client-1 /bin/bash
      dotnet UI.dll
     ```
