from confluent_kafka import Producer
import socket

CONF = {'bootstrap.servers': 'kafka:9092',
        'client.id': socket.gethostname()}


class KafkaProducer:
    def __init__(self):
        self.producer = Producer(CONF)

    def send_message(self, topic, message):
        self.producer.produce(topic, key="key", value=message)
        self.producer.flush()
