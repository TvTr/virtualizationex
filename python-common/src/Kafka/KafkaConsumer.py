import sys
from confluent_kafka import Consumer, KafkaError, KafkaException
import socket


class KafkaConsumer:
    def __init__(self, group_id):
        self.config = {'bootstrap.servers': 'kafka:9092',
        'group.id': group_id,
        'auto.offset.reset': 'smallest'}
        self.consumer = Consumer(self.config)
        self.listening = False

    def __del__(self):
        self.consumer.close()


    def subscribe(self, topics, callback, consume_once=False):
        self.listening = True
        self.__consume(topics, callback, consume_once)

    def unsubscribe(self):
        self.listening = False

    def __consume_once(self, callback):
        msg = self.consumer.poll()
        if msg is not None:
            if msg.error():
                if msg.error().code() == KafkaError._PARTITION_EOF:
                    # End of partition event
                    sys.stderr.write('%% %s [%d] reached end at offset %d\n' %
                                     (msg.topic(), msg.partition(), msg.offset()))
                elif msg.error():
                    raise KafkaException(msg.error())
            else:
                callback(msg)


    def __consume(self, topics, callback, consume_once):
        self.consumer.subscribe(topics)
        while self.listening:
            self.__consume_once(callback)
            if consume_once:
                self.unsubscribe()
